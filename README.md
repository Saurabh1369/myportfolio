# My Portfolio

Developed my personal portfolio using html, scss, svg animations and Javascript libraries ScrollReveal.js, Tilt.js

See Live : https://www.sourabhjoshi.dev

## Developer environment setup
* Install nodeJS ( https://nodejs.org/en/download )
* Clone this project repo
* Do `npm install`
* Run project using `npm run start`
* Prepare prod build using `npm run build`

## Development & CD pipeline
* Developer PRs are merged into `master` branch.
* On release cycle `master` changes are merged into `production` branch. 
* AWS webhook is triggered everytime new commit is merged into `production` branch.
* AWS amplify builds project and deploys it.

## License
Free for personal and commercial use under the MIT License.

#### Code contributions are welcomed