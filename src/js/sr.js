import assignProps from './assignProps';
export default function() {
  const defaultProps = {
    easing: 'cubic-bezier(0.5, 0, 0, 1)',
    distance: '30px',
    duration: 1000,
    desktop: true,
    mobile: true
  };

  // Patch : Add class on scroll animation #498
  // https://github.com/jlmakes/scrollreveal/issues/337#issuecomment-309657013
  ScrollReveal().watch = function (target, onEnter, onExit) {
    onExit = onExit || function () {}
    if (typeof onEnter === 'function' && typeof onExit === 'function') {
      var noEffect = {
        delay:    0,
        distance: 0,
        duration: 0,
        scale:    1,
        opacity:  null,
        rotate:   { x: 0, y: 0, z: 0, },

        reset: true,
        beforeReset: onExit,
        beforeReveal: onEnter,
      }
      this.reveal(target, noEffect)
    } else {
      throw new Error('Watch received invalid arguments.')
    }
  }
  
  /* Section Title */
  ScrollReveal().reveal('.section-title', 
    assignProps(
      {
        delay: 300,
        distance:'0px', 
        origin:'bottom'
      }, defaultProps)
  );

  /* Hero Section */
  ScrollReveal().reveal('.hero-title', 
    assignProps(
      { 
        delay: 500, 
        origin: window.innerWidth > 768 ? 'left' : 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.hero-subtitle', 
  assignProps(
    { 
      delay: 800, 
      origin: window.innerWidth > 768 ? 'left' : 'bottom'
    }, defaultProps)
  );
  
  ScrollReveal().reveal('.hero-cta', 
    assignProps(
      {
        delay: 1000, 
        origin: window.innerWidth > 768 ? 'left' : 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.hero-svg', 
  assignProps(
    { 
      delay: 1500, 
      origin: 'bottom',
      distance : '70px',
    }, defaultProps)
  );

  /* Experience Section */
  ScrollReveal().reveal('.experience-wrapper__org', 
    assignProps(
      {
        delay: 600,
        origin: 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.experience-wrapper__info', 
    assignProps(
      {
        delay: 1000,
        origin: window.innerWidth > 768 ? 'left' : 'bottom'
      }, defaultProps)
  );

  /* Skills Section */
  // adds animate-bar class on reveal of 'bar-expand' class.
  // uses patched 'watch' function.
  ScrollReveal().watch('.bar-expand', 
    function onEnter (element) {
      element.classList.add('animate-bar');
    }
  );

  ScrollReveal().reveal('.skill-icons', 
  assignProps(
    {
      delay: 600,
      origin: 'bottom'
    }, defaultProps)
  );

  /* About Section */
  ScrollReveal().reveal('.about-wrapper__image', 
    assignProps(
      {
        delay: 600,
        origin: 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.about-wrapper__info', 
    assignProps(
      {
        delay: 800,
        origin: window.innerWidth > 768 ? 'left' : 'bottom'
      }, defaultProps)
  );

  /* Projects Section */
  ScrollReveal().reveal('.project-wrapper__text', 
    assignProps(
      {
        delay: 500,
        origin: window.innerWidth > 768 ? 'left' : 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.project-wrapper__image', 
    assignProps(
      {
        delay: 1000,
        origin: window.innerWidth > 768 ? 'right' : 'bottom'
      }, defaultProps)
  );

  /* Contact Section */
  ScrollReveal().reveal('.contact-wrapper', 
    assignProps(
      {
        delay: 800,
        origin: 'bottom'
      }, defaultProps)
  );

  ScrollReveal().reveal('.quote', 
    assignProps(
      {
        delay: 1200,
        origin: 'bottom'
      }, defaultProps)
  );
}
